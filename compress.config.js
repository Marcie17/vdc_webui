/**
 * @file 默认配置
 */

module.exports = {

    // 打包后的文件目录
    root: './dist',

    // 语言包的yaml文件目录，中英文随意，反正中英文的key是一致的
    localeFiles: './static/locale/zh-CN/resource',
};
