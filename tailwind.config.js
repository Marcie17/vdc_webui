module.exports = {
    purge: {
        mode: 'all',
        content: [
            './public/**/*.html',
            './src/**/*.{vue,js,ts,jsx,tsx}',
            './node_modules/@uedc/css-helper/lib/*',
            './node_modules/@sxf/sf-vue-component/dist/static/js/*'
        ]
    },
    theme: {
        extend: {
            inset: {
                0.5 : '0.125rem',
                2 : '0.5rem'
            }
        },
        colors: {
            success: 'var(--color-belarus-green)', // #00B27A
            warning: 'var(--color-golden-yellow)', // #FF9500
            fail: 'var(--color-classical-red)', // #E65050
            note: 'var(--color-gray-d30)', // #999
            subtitle: 'var(--color-gray-d40)', // #666
            link: 'var(--color-blue-70)', // #204ED9;
            'black-l60': 'var(--color-graphite-black-l60)', // #596380
            'yellow-l10': 'var(--color-yellow-l10)', // #FFC53D
            'graphite-black-l70': 'var(--color-graphite-black-l70)', // #626D8C 进度条颜色
            baseborder: 'var(--base-border)', // border颜色
            'gray-l40': 'var(--color-gray-l40)', // #F6F6F6
            'gray-d50': 'var(--color-gray-d50)', // #333
            'gray-l60': 'var(--color-gray-l60)', // #FFF
            icon: 'var(--color-brand-lime-d30)', // icon石墨灰 #444B55
            'gray-d1':'var(--color-gray-d1)', // icon禁用色#bbb
        },
        minWidth: {
            'security-manage': '1340px', // 准入安全策略最小宽度
            'resource-manage': '1200px', // 云桌面最小宽度
        }
    },
    plugins: [],
};