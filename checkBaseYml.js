const nodePath = require('path');
const YAML = require('yaml');
const nodeFs = require('fs');
const WHITE_LIST = require('./checkBaseYmlWhiteList');


// 检查是否存在英文特殊字符
function checkSpecialChar () {
    const zhCNDir = nodePath.resolve(__dirname, 'static/locale/zh-CN/resource/base.yml');
    let yml = YAML.parse(nodeFs.readFileSync(zhCNDir, 'utf-8'));

    const SPECIAL_CHAR = /[,:!;?]/;

    const keys = Object.keys(yml);

    let arr = keys.reduce((prev, key) => {
        let str = yml[key];
        // 不在白名单内且存在特殊字符
        if (!(WHITE_LIST.hasOwnProperty(key) && WHITE_LIST[key] == str) && SPECIAL_CHAR.test(str)) {
            prev[key] = str;
        }
        return prev;
    }, {});

    // let arr = WHITE_LIST.reduce((prev, v) => {
    //     let str = yml[v];
    //     if (str) {
    //         prev[v] = str;
    //     }
    //     return prev;
    // }, {});

    console.log(arr);
    console.log('count: ', Object.keys(arr).length)
}


// 检索代码中使用的翻译词条
function checkWordUsed () {
    let filePath = 'src';

    // 用于找出引用i18n/index库的文件
    let lookingForString = "'i18n/index'";

    // 匹配文件中tr('xxxx.xxx'、$i('xxxxx.xxx'等格式的翻译代码段
    let codeReg = /(\$i\('[a-z1-9.]{0,}')|(\$i\("[a-z1-9.]{0,}")|(tr\('[a-z1-9.]{0,}')|(tr\("[a-z1-9.]{0,}")/g;

    // 所有匹配出的词条数组
    let arr = [];

    // arr去重后的词条数组
    let list = [];
    recursiveReadFile(filePath);

    // 去重
    list = arr.filter((item, index) => arr.indexOf(item) === index);
    console.log(list, list.length);

    // 递归遍历
    function recursiveReadFile (fileName) {
        
        if (!nodeFs.existsSync(fileName)) return;
        if (isFile(fileName)) {
            check(fileName);
        }
        if (isDirectory(fileName)) {
            var files = nodeFs.readdirSync(fileName);
            files.forEach(function (val, key) {
                var temp = nodePath.join(fileName, val);
                if (isDirectory(temp)) recursiveReadFile(temp);
                if (isFile(temp)) check(temp);
            })
        }
    }

    // 正则匹配并截取对应词条
    function check (fileName) {
        let data = readFile(fileName);
        let exc = new RegExp(lookingForString);
        if (exc.test(data)){
            let res = data.match(codeReg);
            if (res && res[0]) {
                let temp = res.reduce((prev, item) => {
                    let start = item.indexOf('\'') >= 0 ? item.indexOf('\'') : item.indexOf('"');
                    let end = item.lastIndexOf('\'') >= 0 ? item.lastIndexOf('\'') : item.lastIndexOf('"');
                    let keyStr = item.substring(start + 1, end);
                    prev.push(keyStr);
                    return prev;
                }, [])
                arr.push(...temp);
            }
        }
    }


    function isDirectory (fileName) {
        if (nodeFs.existsSync(fileName)) return nodeFs.statSync(fileName).isDirectory();
    }
    function isFile (fileName) {
        if (nodeFs.existsSync(fileName)) return nodeFs.statSync(fileName).isFile();
    }
    function readFile (fileName) {
        if (nodeFs.existsSync(fileName)) return nodeFs.readFileSync(fileName, "utf-8");
    }
}
