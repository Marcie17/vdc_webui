module.exports = {
  ci: {
    collect: {
      url: [
        // 这里指定要访问的页面地址
        "https://localhost:2346/html/web/index_lhci.html#/",
      ],
      settings: {
        formFactor: "desktop", // 相当于 lighthouse 面板的 Device 项
        screenEmulation: {
          mobile: false,
          width: 1920,
          height: 1080,
          deviceScaleFactor: 1,
          disabled: false,
        },
        throttling: {
          // 需要配置成 4G 网络
          rttMs: 40,
          throughputKbps: 10 * 1024,
          cpuSlowdownMultiplier: 1,
          requestLatencyMs: 0, // 0 means unset
          downloadThroughputKbps: 0,
          uploadThroughputKbps: 0,
        },
        // --disable-dev-shm-usage 需要配置，不然再docker容器下跑分会失败
        chromeFlags:
          "--no-sandbox --disable-dev-shm-usage --ignore-certificate-errors",
      },
      startServerCommand: "npx local-server start",
      numberOfRuns: 3, // 配置每个页面地址 跑lighthouse 的次数，可减少数据的波动
    },

    // 忽略断言
    assert: {
      budgetsFile: "./budget.json"
    },
    upload: {
      target: "lhci",
      serverBaseUrl: "http://200.200.42.87:5124", // 数据上传到哪个服务器
      // Created project vdc_webui (a1d24c1f-b50b-495d-a4f2-4cc8a48556f6)!
      // Use build token 37dff7f8-9e2f-49cb-b506-fceb8cd19f86 to add data.
      // Use admin token EOeDzEBc6IQme5x2RfhB7GcXuroO13LW5HY9Bf6u to manage data. KEEP THIS SECRET!

      // 上传数据用的 build token，由后面 lhci wizard 命令 生成
      token: "37dff7f8-9e2f-49cb-b506-fceb8cd19f86",
    },
  },
};
