import '../../static/locale/polyfill.min.js';
import '../../static/locale/zh-CN/polyfill.min.js';
// import '../../static/locale/zh-CN/resource/index.css';

// 因为需要同时兼容vue-cli 和 vite 无法使用构建工具自动加载的特性，只能手动加载
import ymlBase from '../../static/locale/zh-CN/resource/base.yml';

const langFiles = [
    ymlBase,
];
export const messages = Object.values(langFiles).reduce((messages, module) => {
    return {
        ...messages,
        ...(module.default || module),
    };
}, {});
export const timeZone = 'Asia/Shanghai';
export const locale = 'zh-CN';
