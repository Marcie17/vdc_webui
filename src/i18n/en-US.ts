import '../../static/locale/polyfill.min.js';
import '../../static/locale/en-US/polyfill.min.js';
// import '../../static/locale/en-US/resource/index.css';

// 因为需要同时兼容vue-cli 和 vite 无法使用构建工具自动加载的特性，只能手动加载
// import app from '../../static/locale/en-US/resource/app.yml';
import base from '../../static/locale/en-US/resource/base.yml';
// todo: 临时加的，为了无en显示cn
// import CN_base from '../../static/locale/zh-CN/resource/base.yml';

const langFiles = [
    // CN_base, 
    // app, 
    base, 
];
export const messages = Object
    .values(langFiles)
    .reduce((messages, module) => {
        return {
            ...messages,
            ...module.default || module
        };
    }, {});

export const timeZone = 'America/New_York';
export const locale = 'en-US';