# vdc-next开发流程

## 环境准备
- node: 12+
- vscode: 1.55+
- eslint: 2.2.2+
- volar: 0.28+
- json2ts: 0.06+
其他插件通过Visual Studio Marketplace下载后，使用vsix方式安装

## 阅读代码规范、GIT规范

#### 代码规范

http://uedc.sangfor.org/static/static-page/checklist/#/

#### commit 规范
feat: ${desc}: 添加模块/功能

fix: ${desc}: 修复 bug

perf: ${desc}: 修改模块/功能

merge: ${desc}: 合入代码/分支

其他不常用参考：
https://www.conventionalcommits.org/zh-hans/v1.0.0/#%e7%ba%a6%e5%ae%9a%e5%bc%8f%e6%8f%90%e4%ba%a4%e8%a7%84%e8%8c%83

## 阅读预开发模块的业务梳理文档，了解模块的业务
目的是了解业务，保障需求无遗漏，除了阅读文档外，也要结合老页面操作和代码去了解业务
http://docs.sangfor.org/pages/viewpage.action?pageId=75365129
## 开发静态页面
1. 基础组件（弹窗、表单项、表格等）用sfv：http://uedc.sangfor.org/block/#/widget/introduce?theme=security&lang=zh_CN

2. sfv没有的，找找 http://uedc.sangfor.org/common/docs/cloud-components/components/，看有没有可用的（**目前为装饰器写法，待转成compostion api写法-周凯抽取高频的**）

3. 接着找找vdc公共组件：http://docs.sangfor.org/pages/viewpage.action?pageId=156211462

4. 开发群里问问有没有类似的实现

5. 最后才自己封装

## 根据后端接口编写接口的类型定义
**目前采取json2ts生成 + 手动编写类型定义（一个controller对应一个api-class、一个api-type）；后面通过脚本或者插件实现-周凯（待定）**
1. 抓取接口数据，ctrl+c，此时数据在剪贴板中
2. 在创建好的xxx.d.ts中ctrl+alt+v，此时json2ts插件会读取剪贴板中的json，然后生成类型定义

## 自测
保障没有基本功能问题，页面显示正常

## 上库
项目代码上库前需要经过 commit hook 检查，通过后才可以上库。